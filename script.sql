CREATE USER 'drupal'@'localhost' IDENTIFIED BY 'YourStrongPasswordHere'; 
CREATE DATABASE drupal; 
GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'localhost'; 
FLUSH PRIVILEGES;
EXIT;
